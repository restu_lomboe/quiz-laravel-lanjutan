<?php

namespace App\Listeners;

use App\Events\TodoCreatedEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class LogInfo
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TodoCreatedEvent  $event
     * @return void
     */
    public function handle(TodoCreatedEvent $event)
    {
        // dd([$event->todo->user->name]);
        // \Log::info($event->todo->user, "berhasil menambahkan todo list");
        \Log::info('berhasil menambahkan todo list', ['name' => $event->todo->user->name]);
    }
}
